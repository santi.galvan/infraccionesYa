var bootstrap = function() {

  var url = Config.url;
	var urlGruas = '/gruas/';
	var urlEstados = '/estadosGruas/';

  var map = createMap('mapaGrua');

  var gruaIcon = L.Icon.extend({
    options: {
        iconSize:     [36, 31],
        shadowSize:   [50, 64],
        iconAnchor:   [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor:  [-3, -76]
    }
  });

  var resolverIcono = function(algo){
    var gruaCargadaIcon = new gruaIcon({iconUrl: 'leaflet/images/gruaCargada.png'}),
        gruaDisponibleIcon = new gruaIcon({iconUrl: 'leaflet/images/gruaDispoinible.png'}),
        gruaNoOperativaIcon = new gruaIcon({iconUrl: 'leaflet/images/gruaNoOperativa.png'});

    if(algo === "Disponible."){
      return new gruaIcon({iconUrl: 'leaflet/images/gruaDispoinible.png'});
    }
    else if(algo === "No operativa."){
      return gruaNoOperativaIcon = new gruaIcon({iconUrl: 'leaflet/images/gruaNoOperativa.png'});
    }
    else{
      return gruaCargadaIcon = new gruaIcon({iconUrl: 'leaflet/images/gruaCargada.png'});
    } 
  }

  var requestCantidadGruas = function(){
    return $.ajax(url+urlGruas+'/posiciones');
  }

  var requestPosiciones = function(grua_id){
    return $.ajax(url+urlGruas+grua_id+'/posiciones');
  }
  //36434793
  var requestEstados = function() {
      return $.ajax(url + urlEstados);
  }

  var resolverEstadoGrua = function(posiciones){
      return requestEstados()
      .then(function(response){

        //Este codigo parece de intro a la progra
        //Lo que hace es reemplazar el estado de la grua que es un numero
        //Por el estado que recibe del sv que es una descripcion
        //Refactorizar URGENTE
        for(var i = 0; i < posiciones.posiciones.length;i++){
          for(var k = 0; k < response.estados.length; k++){
            if(posiciones.posiciones[i].estado ==  response.estados[k].id){
              posiciones.posiciones[i].estado = response.estados[k].descripcion;
            }
          }
        }
          return posiciones;
        })
      }

  var correr = function()
  {
    var totalGruas = 6;

    for (var i = 1; i <= totalGruas; i++) {
      requestPosiciones(i).
      then(resolverEstadoGrua).
      then(function(response){

        var gruaLayer = L.layerGroup().addTo(map);
        map.layersControl.addOverlay(gruaLayer, response.grua);

        var estadosDeLasGruas = document.getElementById('estadosDeLasGruas');
        var estadoActual = document.createElement('li');
        estadosDeLasGruas.appendChild(estadoActual);


        for(var i = 0; i < response.posiciones.length; i++){
            (function(ind){
              setTimeout(function(){
                //Listo estado de la grua en pantalla
                estadoActual = document.createElement('li');
                estadoActual.appendChild(document.createTextNode("Grua " + response.grua+ ": "));
                estadoActual.appendChild(document.createTextNode(response.posiciones[ind].estado));
                estadoActual.appendChild(document.createElement('br'));
                estadoActual.appendChild(document.createElement('br'));

                estadosDeLasGruas.replaceChild(estadoActual, estadosDeLasGruas.childNodes[response.grua]);
                
                  //Dibujo en el mapa
                gruaLayer.clearLayers(); 
                var icono = resolverIcono(response.posiciones[ind].estado);                                                                              //De aca se cambia la velocidad
                gruaLayer.addLayer(L.marker(response.posiciones[ind].ubicacion, {icon: icono}).bindPopup(''+response.posiciones[ind].estado));}, 1000 + 500*ind);
                })(i);
        }
      })
    }
  }

  correr();



};

$(bootstrap);
