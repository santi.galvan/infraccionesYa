var Recorrido = function(name, map) {
    this.name = name;
    this.map = map;
    this.gruasData = [];

    this.addgrua = function(grua) {
        //Creamos el layer en el mapa para ese grua
        var gruaLayer = L.layerGroup().addTo(this.map);
        // Agregamos el layer al control
        this.map.layersControl.addOverlay(gruaLayer, grua.name);

        var updater = function(newPosition) {
            console.log("Actualizando posición de grúa: " + grua.name + ".");
            console.log(newPosition);

            gruaLayer.clearLayers();
            gruaLayer.addLayer(L.marker(newPosition).bindPopup(grua.name));
        }

        this.gruasData.push({
            grua: grua,
            updater: updater
        })
    }

    this.start = function() {
        this.gruasData.forEach(function(data) {
            var grua = data.grua;
            grua.run(data.updater);
        });
    }
};
