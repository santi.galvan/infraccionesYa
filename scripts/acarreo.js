
function bootstrap() {
    //patente/acarreos/:infraccion_id":{"
    var url = Config.url;
    var urlAcarreo = '/acarreos/';
    var map = createMap('mapaAcarreo');


    var corroborarPatente = function(numeroPatente){

    }

    var vehiculoIcon = L.Icon.extend({
      options: {
          iconSize:     [36, 31],
          shadowSize:   [50, 64],
          iconAnchor:   [22, 94],
          shadowAnchor: [4, 62],
          popupAnchor:  [-3, -76]
      }
    });

    var requestPatente = function(numeroPatente) {
        return $.ajax(url + numeroPatente + '/infracciones/');
    }

    var requestAcarreo = function(infraccion) {
        return $.ajax(url + infraccion.patente+''+urlAcarreo+infraccion.id);
     }

    var extraerIdAcarreo = function(infracciones){

        var idInfraccion;

        for(var i =0; i < infracciones.infracciones.length; i ++){
          if(infracciones.infracciones[i].existeAcarreo)
            idInfraccion = infracciones.infracciones[i];
        }

        return idInfraccion;

     }

     var resolverUbicacionVehiculo = function(idInfraccion){
       return requestAcarreo(idInfraccion);
     }

     var mostrarUbicacion = function(acarreo){
       console.log(acarreo);
       var icono = new vehiculoIcon({iconUrl: 'leaflet/images/auto.png'});
       var p = L.marker(L.latLng(acarreo.acarreo.deposito.ubicacion.lat, acarreo.acarreo.deposito.ubicacion.lon), {icon: icono}).bindPopup('Deposito !!! ');
       p.addTo(map);
     }

     var mostrarVehiculoMapa = function(){

       var numeroPatente = document.getElementById('receptorPatente').value;

       corroborarPatente(numeroPatente);

       requestPatente(numeroPatente)
       .then(extraerIdAcarreo)
       .then(resolverUbicacionVehiculo)
       .then(mostrarUbicacion)
       .then(function(){
         console.log('Fin');
       })


     }

     document.getElementById('enviadorPatente').addEventListener('click', mostrarVehiculoMapa);

}
$(bootstrap);
