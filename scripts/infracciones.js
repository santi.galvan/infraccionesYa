var bootstrap = function() {

    var url = Config.url;
  	var urlInfracciones = '/infracciones/';
    var urlTipoInfracciones = '/tiposInfraccion';

    var consultarInfracciones = function(numeroPatente) {
        return $.ajax(url + numeroPatente + urlInfracciones);
    }

    var consultarTiposDeInfracciones = function(){
        return $.ajax(url + urlTipoInfracciones);
    }

    var resolverTiposDeInfracciones = function(infracciones){
      return consultarTiposDeInfracciones()
            .then(function(response){
            //REFACTORIZAR
            for(var i = 0; i < infracciones.infracciones.length;i ++){
              for(var k = 0; k < response.tipos.length; k++){
                if(infracciones.infracciones[i].tipoInfraccion == response.tipos[k].id){
                    infracciones.infracciones[i].tipoInfraccion = response.tipos[k].descripcion;
                }
              }
            }
            return infracciones;
          })
    }

    var listarInfraccionesEnPantalla = function(response){

      var divInfracciones= document.getElementById('cuadroInfracciones');
      var listaInfracciones = document.createElement('ul');
      //Si hay un ul ya cargado, borrarlo y actualizar la informacion
        response.infracciones.forEach(function(inf) {
          var elemento = document.createElement('li');
          elemento.appendChild(document.createTextNode( 'Id: '+ inf.id));
          elemento.appendChild(document.createElement('br'));

          var itemFechaHora = document.createElement('li');
          elemento.appendChild(document.createTextNode('Fecha y hora de registro: '+ inf.fechaHoraRegistro));
          elemento.appendChild(document.createElement('br'));

          var itemMonto = document.createElement('li').appendChild(document.createTextNode('Monto a pagar: '+inf.montoAPagar));
          elemento.appendChild(itemMonto);
          elemento.appendChild(document.createElement('br'));

          var itemDirRegistrada = document.createElement('li').appendChild(document.createTextNode('Dirección registrada: '+inf.direccionRegistrada));
          elemento.appendChild(itemDirRegistrada);
          elemento.appendChild(document.createElement('br'));

          var itemTipoInfraccion = document.createElement('li').appendChild(document.createTextNode( 'Tipo de Infracción: '+ inf.tipoInfraccion));
          elemento.appendChild(itemTipoInfraccion);
          elemento.appendChild(document.createElement('br'));

          if(inf.existeAcarreo){

            var boton = document.createElement('input');
            boton.id = 'botonConsultaAcarreo';
            boton.type='button';

            boton.addEventListener('click',function(){
              /*La idea era poder redireccionar a la página
                Y que muestre la ubicacion del vehiculo en el mapa
                Sin tener que cargar los datos nuevamente
              */
              location.href='acarreo.html'
            })
            elemento.appendChild(boton);
          }

          listaInfracciones.appendChild(elemento);

      });
      divInfracciones.appendChild(listaInfracciones);
    }

    var corroborarPatente = function(numeroPatente){

    }

    var corroborarDiv = function(){

    }

    var traerInfracciones = function(){

      var numeroPatente = document.getElementById('receptorPatente').value;

      corroborarPatente(numeroPatente);
      corroborarDiv();

      consultarInfracciones(numeroPatente)
      .then(resolverTiposDeInfracciones)
      .then(listarInfraccionesEnPantalla)
    }

    document.getElementById('enviadorPatente').addEventListener('click',traerInfracciones);

}
$(bootstrap);
